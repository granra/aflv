#include <iostream>
#include <vector>

int main()
{
  int n, m;
  std::cin >> n >> m;

  while (n != 0 && m != 0)
  {

    std::vector<long> arr;

    for (int i = 0; i < n; ++i)
    {
      long c;
      std::cin >> c;

      arr.push_back(c);
    }

    int count = 0;

    for (int i = 0; i < m; ++i)
    {
      long c;
      std::cin >> c;

      std::vector<long>::iterator it = lower_bound(arr.begin(), arr.end(), c);
      if (*it == c) count++;
    }

    std::cout << count << std::endl;

    std::cin >> n >> m;
  }

  return 0;
}

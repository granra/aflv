#include <iostream>
#include <map>

using namespace std;

int main()
{
  map<string, int> words;
  int m, n;

  cin >> m >> n;

  for (int i = 0; i < m; ++i)
  {
    string word;
    int score;

    cin >> word >> score;

    words.insert(pair<string, int>(word, score));
  }

  for (int i = 0; i < n; ++i)
  {
    string word;
    cin >> word;
    int overall = 0;
    while (word != ".")
    {
      map<string, int>::iterator it = words.find(word);
      if (it != words.end()) overall += it->second;
      cin >> word;
    }

    cout << overall << endl;
  }
}

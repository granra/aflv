#include <stdio.h>
#include <vector>
#include <string.h>
#include <algorithm>
#include <queue>

using namespace std;

int getMedian(priority_queue<int, vector<int>, greater<int> > &top, priority_queue<int> &bottom)
{
  int ret;
  if (bottom.size() > top.size())
  {
    ret = bottom.top();
    bottom.pop();
  }
  else // (top.size() == bottom.size()) || (top.size() > bottom.size())
  {
    ret = top.top();
    top.pop();
  }

  return ret;
}

void insert(priority_queue<int, vector<int>, greater<int> > &top, priority_queue<int> &bottom, int n)
{
  if (bottom.empty() || n < bottom.top())
    bottom.push(n);
  else
    top.push(n);

  // Making the size equal if one is two elements larger
  if (bottom.size() - top.size() == 2)
  {
    top.push(bottom.top());
    bottom.pop();
  }
  else if (top.size() - bottom.size() == 2)
  {
    bottom.push(top.top());
    top.pop();
  }
}

int main()
{
  priority_queue<int, vector<int>, greater<int> > top;
  priority_queue<int> bottom;
  char str[10];
  while (scanf("%9s", str) != EOF)
  {
    if (strcmp(str, "#") != 0)
    {
      int n = atoi(str);
      insert(top, bottom, n);
    }
    else
    {
      printf("%d\n", getMedian(top, bottom));
    }
  }
}

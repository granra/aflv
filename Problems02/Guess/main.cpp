#include <iostream>
#include <queue>
#include <stack>

using namespace std;

int main()
{
  int n;

  while (cin >> n)
  {
    stack<int> st;
    queue<int> q;
    priority_queue<int> pq;
    int set = 0b111;

    for (int i = 0; i < n; ++i)
    {
      int k, l;
      cin >> k >> l;

      if (k == 1)
      {
        st.push(l);
        q.push(l);
        pq.push(l);
      }
      else if (k == 2)
      {
        if (!st.empty())
        {
          if (st.top() != l)  set &= ~(1 << 2);
          if (q.front() != l)   set &= ~(1 << 1);
          if (pq.top() != l)  set &= ~1;
          st.pop();
          q.pop();
          pq.pop();
        }
        else set = 0;
      }
    }

    switch (set)
    {
      case 0:
        cout << "impossible" << endl;
        break;
      case 0b001:
        cout << "priority queue" << endl;
        break;
      case 0b010:
        cout << "queue" << endl;
        break;
      case 0b100:
        cout << "stack" << endl;
        break;
      default:
        cout << "not sure" << endl;
    }
  }

  return 0;
}

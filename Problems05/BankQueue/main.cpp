#include <cstdio>
#include <vector>
#include <queue>

using namespace std;

int main()
{
    int N, T;
    while (scanf("%d %d", &N, &T) != EOF)
    {
        int sum = 0;
        vector<priority_queue<int> > people(T);
        for (int i = 0; i < N; ++i)
        {
            int c, t;
            scanf("%d %d", &c, &t);
            people[t].push(c);
        }

        for (int i = T - 1; i >= 0; --i)
        {
            int biggest = 0, biggest_j = -1;
            for (int j = T - 1; j >= i; --j)
            {
                if (!people[j].empty() && people[j].top() > biggest)
                {
                    biggest = people[j].top();
                    biggest_j = j;
                }
            }
            if (biggest_j != -1) people[biggest_j].pop();
            sum += biggest;
        }
        printf("%d\n", sum);
    }
}

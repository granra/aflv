#include <cstdio>
#include <queue>

using namespace std;

int main()
{
    int n;
    while (scanf("%d", &n) != EOF)
    {
        priority_queue<int> plants;
        priority_queue<int> end;
        for (int i = 0; i < n; ++i)
        {
            int p;
            scanf("%d", &p);
            plants.push(p);
        }
        for (int i = 1; i <= n; ++i)
        {
            end.push(i + plants.top());
            plants.pop();
        }
        printf("%d\n", end.top() + 1);
    }
}

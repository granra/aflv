#include <iostream>
#include <algorithm>

using namespace std;

int main()
{
    string A, B;
    while (cin >> A >> B)
    {
        int start = -1, end = -1, Alength = A.length(), Blength = B.length();
        for (int i = 0; i < Alength && i < Blength; ++i)
        {
            if (A[i] != B[i])
            {
                start = i;
                break;
            }
        }
        if (start == -1)
        {
            if (Alength < Blength)
            {
                cout << Blength - Alength << endl;
                continue;
            }
            else
            {
                cout << 0 << endl;
                continue;
            }
        }
        for (int i = Alength - 1, j = Blength - 1; i >= start - 1; --i, --j)
        {
            if (i < 0 || A[i] != B[j])
            {
                end = j + 1;
                break;
            }
        }
        if (end - start < 0) cout << 0 << endl;
        else                 cout << end - start << endl;
    }
    return 0;
}

#include <iostream>

int countA(std::string in)
{
  int i = 0;
  while(in[i] != 'h')
  {
    if(in[i] == 'a') i++;
  }
  return i;
}

int main()
{
  std::string man, doc;
  std::cin >> man;
  int j = countA(man);
  std::cin >> doc;
  int k = countA(doc);

  if(j > k) std::cout << "Go";
  else      std::cout << "no";
}

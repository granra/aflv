#include <iostream>
#include <sstream>

int main()
{
  int n;

  for(int k = 1; std::cin >> n; ++k)
  {
    int min = 1000000, max = -1000000;
    for(int i = 0; i < n; ++i)
    {
      int j;
      std::cin >> j;
      if(j < min) min = j;
      if(j > max) max = j;
    }
    std::cout << "Case " << k << ": " << min << " " << max << " " << max - min << std::endl;
  }
}

#include <iostream>

int getIndex(std::string in)
{
  if     (in == "thou"    || in == "th")    return 0;
  else if(in == "inch"    || in == "in")    return 1;
  else if(in == "foot"    || in == "ft")    return 2;
  else if(in == "yard"    || in == "yd")    return 3;
  else if(in == "chain"   || in == "ch")    return 4;
  else if(in == "furlong" || in == "fur")   return 5;
  else if(in == "mile"    || in == "mi")    return 6;
  else if(in == "league"  || in == "lea")   return 7;
}

double convert(double amount, std::string from, std::string to)
{
  int conversions[7] = {1000, 12, 3, 22, 10, 8, 3};
  int i = getIndex(from), j = getIndex(to);

  if(i < j)
    for(; i < j; ++i) amount /= conversions[i];
  else
    for(; j < i; ++j) amount *= conversions[j];

  return amount;
}

int main()
{
  double amount;
  std::string from, in, to;

  std::cin >> amount >> from >> in >> to;

  std::cout.precision(13);
  std::cout << convert(amount, from, to);
}

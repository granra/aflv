#include <iostream>

int main()
{
  long x, y;

  while(std::cin >> x)
  {
    std::cin >> y;
    if(x < y) std::cout << y - x << std::endl;
    else      std::cout << x - y << std::endl;
  }

  return 0;
}

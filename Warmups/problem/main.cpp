#include <iostream>
//#include <cstdio>

char toLower(char c)
{
  if(c < 91 && c > 64) return c + 32;
  return c;
}

bool contains(std::string input, std::string word)
{
  for(int i = 0; i < input.length(); ++i)
  {
    if(toLower(input[i]) == toLower(word[0]))
    {
      ++i;
      for(int j = 1; j < word.length() && i < input.length(); ++j, ++i)
      {
        if(toLower(input[i]) != toLower(word[j])) break;
        else if(j == word.length() - 1) return 1;
      }
    }
  }
  return 0;
}

int main()
{
  //freopen("sample.in", "r", stdin);
  std::string input;

  while(std::getline(std::cin, input))
  {
    if(contains(input, "problem"))  std::cout << "yes";
    else                            std::cout << "no";
    std::cout << std::endl;
  }
}

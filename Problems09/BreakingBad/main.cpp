#include <iostream>
#include <map>
#include <vector>

using namespace std;

vector<int> adj[100000];
vector<int> side(100000, -1);
bool is_bipartite = true;

void check_bipartite(int u) {
    for (int i = 0; i < adj[u].size(); i++) {
        int v = adj[u][i];
        if (side[v] == -1) {
            side[v] = 1 - side[u];
            check_bipartite(v);
        } else if (side[u] == side[v]) {
            is_bipartite = false;
        }
    }
}

int main()
{
    int n, m;
    cin >> n;
    map<string, int> items;
    vector<string>   item_names(n);
    for (int i = 0; i < n; ++i)
    {
        string item;
        cin >> item;
        items[item] = i;
        item_names[i] = item;
    }

    cin >> m;
    for (int i = 0; i < m; ++i)
    {
        string a, b;
        cin >> a >> b;
        int j, k;
        j = items[a];
        k = items[b];
        adj[j].push_back(k);
        adj[k].push_back(j);
    }
    for (int u = 0; u < n; u++) {
        if (side[u] == -1) {
            side[u] = 0;
            check_bipartite(u);
        }
    }
    if (!is_bipartite)
    {
        cout << "impossible" << endl;
        return 0;
    }

    vector<string> walter, jesse;
    for (int i = 0; i < n; ++i)
    {
        if (side[i] == 0) walter.push_back(item_names[i]);
        else if (side[i] == 1) jesse.push_back(item_names[i]);
    }
    for (int i = 0; i < walter.size(); ++i)
    {
        cout << walter[i] << " ";
    }
    cout << endl;
    for (int i = 0; i < jesse.size(); ++i)
    {
        cout << jesse[i] << " ";
    }
    cout << endl;

    return 0;
}

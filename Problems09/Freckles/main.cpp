#include <cstdio>
#include <vector>
#include <utility>
#include <cmath>
#include <algorithm>

using namespace std;

struct union_find {
    vector<int> parent;
    union_find(int n) {
        parent = vector<int>(n);
        for (int i = 0; i < n; i++) {
            parent[i] = i;
        }
    }
    int find(int x) {
        if (parent[x] == x) {
            return x;
        } else {
            parent[x] = find(parent[x]);
            return parent[x];
        }
    }
    void unite(int x, int y) {
        parent[find(x)] = find(y);
    }
};

double getDist(pair<double, double> a, pair<double, double> b)
{
    double x = abs(a.first - b.first);
    double y = abs(a.second - b.second);
    return sqrt((x*x) + (y*y));
}

struct edge {
    int u, v;
    double weight;
    edge(int _u, int _v, double _w) {
        u = _u;
        v = _v;
        weight = _w;
    }
};
bool edge_cmp(const edge &a, const edge &b) {
    return a.weight < b.weight;
}
vector<edge> mst(int n, vector<edge> edges) {
    union_find uf(n);
    sort(edges.begin(), edges.end(), edge_cmp);

    vector<edge> res;
    for (int i = 0; i < edges.size(); i++) {
        int u = edges[i].u,
            v = edges[i].v;

        if (uf.find(u) != uf.find(v)) {
            uf.unite(u, v);
            res.push_back(edges[i]);
        }
    }
    return res;
}

int main()
{
    int t;
    scanf("%d", &t);

    for (int i = 0; i < t; ++i)
    {
        int n;
        scanf("%d", &n);
        vector<pair<double, double> > coords(n);
        vector<edge> edges;
        for (int j = 0; j < n; ++j)
        {
            double x, y;
            scanf("%lf %lf", &x, &y);
            coords[j] = make_pair(x, y);
            for (int k = 0; k < j; ++k)
            {
                edges.push_back(edge(j, k, getDist(coords[j], coords[k])));
            }
        }
        vector<edge> paths_taken = mst(n, edges);
        double sum = 0.0;
        for (int j = 0; j < paths_taken.size(); ++j)
        {
            sum += paths_taken[j].weight;
        }
        printf("%.2lf\n", sum);
    }

    return 0;
}

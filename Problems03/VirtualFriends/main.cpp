#include <iostream>
#include <map>
#include <string>
#include <stdexcept>

using namespace std;

#define MAXN 200000

struct union_find
{
    int p[MAXN];
    int children[MAXN];
    int cnt;
    map<string, int> people;
    union_find()
    {
        cnt = 0;
        for (int i = 0; i < MAXN; ++i)
        {
            p[i] = i;
            children[i] = 0;
        }
    }
    int find(int x)
    {
        if (p[x] == x) {
            return x;
        } else {
            p[x] = find(p[x]);
            return p[x];
        }
    }
    int unite(string a, string b)
    {
        int x, y;
        if (people.find(a) != people.end()) {
            x = people[a];
        } else {
            x = cnt;
            people.insert( pair<string, int>(a, cnt++) );
        } if (people.find(b) != people.end()) {
            y = people[b];
        } else {
            y = cnt;
            people.insert( pair<string, int>(b, cnt++) );
        }
        int x_root = find(x), y_root = find(y);
        if (x_root != y_root)
        {
            p[x_root] = y_root;
            children[y_root] += children[x_root] + 1;
        }
        return y_root;
    }
    int count(int x)
    {
        return children[x] + 1;
    }
};

int unite_friends(string a, string b, union_find &uf)
{
    int x = uf.unite(a, b);
    return uf.count(x);
}

int main()
{
    int n;
    cin >> n;
    for (int i = 0; i < n; ++i)
    {
        union_find uf;
        int f;
        cin >> f;
        for (int j = 0; j < f; ++j)
        {
            string a, b;
            cin >> a >> b;
            cout << unite_friends(a, b, uf) << endl;
        }
    }
}

#include <stdio.h>
#include <vector>
#include <utility>
#include <cmath>

using namespace std;

struct union_find {
    vector<int> parent;
    union_find(int n) {
        parent = vector<int>(n);
        for (int i = 0; i < n; i++) {
            parent[i] = i;
        }
    }
    int find(int x) {
        if (parent[x] == x) {
            return x;
        } else {
            parent[x] = find(parent[x]);
            return parent[x];
        }
    }
    void unite(int x, int y) {
        parent[find(x)] = find(y);
    }
    bool reachable(int x, int y)
    {
        return find(x) == find(y);
    }
};

bool withinReach(int first_x, int first_y, int second_x, int second_y)
{
    int ret = abs(first_x - second_x) + abs(first_y - second_y);
    return ret <= 1000;
}

int main()
{
    int t;
    scanf("%d", &t);
    for (int i = 0; i < t; ++i)
    {
        int n;
        scanf("%d", &n);
        n += 2;
        vector<pair<int, int> > arr(n);
        union_find uf(n);
        for (int j = 0; j < n; ++j)
        {
            int x, y;
            scanf("%d %d", &x, &y);
            arr[j] = pair<int, int>(x, y);
            for (int k = 0; k < j; ++k)
            {
                if (withinReach(arr[j].first, arr[j].second, arr[k].first, arr[k].second))
                    uf.unite(k, j);
            }
        }

        if (uf.reachable(0, n - 1)) printf("happy\n");
        else                        printf("sad\n");
    }
}

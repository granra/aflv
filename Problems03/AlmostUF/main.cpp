#include <stdio.h>

using namespace std;

struct union_find
{
    int* p;
    int* children;
    int* sum;
    int max_size;
    union_find(int x)
    {
        x++;
        max_size = x;
        p = new int[x];
        children = new int[x];
        sum = new int[x];
        for (int i = 0; i < x; ++i)
        {
            p[i] = i;
            sum[i] = i;
            children[i] = 0;
        }
    }
    ~union_find()
    {
        delete[] p;
        delete[] children;
        delete[] sum;
    }
    int find(int x)
    {
        return p[x];
    }
    bool inRange(int x, int y)
    {
        return x < max_size && y < max_size;
    }
    bool isRoot(int x)
    {
        return p[x] == x;
    }
    void unite(int x, int y)
    {
        int x_root = find(x), y_root = find(y);
        if (x_root != y_root && inRange(x, y))
        {
            for (int i = 0; i < max_size; ++i)
            {
                if (p[i] == x_root)
                {
                    p[i] = y_root;
                    sum[i] = i;
                    children[i] = 0;
                    sum[y_root] += i;
                    children[y_root]++;
                }
            }
        }
    }
    void move(int x, int y)
    {
        int x_root = find(x), y_root = find(y);
        if (x_root != y_root && inRange(x, y))
        {
            if (isRoot(x))
            {
                for (int i = 0; i < max_size; ++i)
                {
                    if (p[i] == x && i != x)
                    {
                        p[i] = i;
                        sum[i] = i;
                        children[i] = 0;
                        for (int j = i + 1; j < max_size; ++j)
                        {
                            if (p[j] == x && j != x)
                            {
                                p[j] = i;
                                sum[j] = j;
                                children[j] = 0;
                                sum[i] += j;
                                children[i]++;
                            }
                        }
                        break;
                    }
                }
                sum[x] = x;
                children[x] = 0;
            }
            else
            {
                sum[x_root] -= x;
                children[x_root]--;
            }
            p[x] = y_root;
            sum[y_root] += x;
            children[y_root]++;
        }
    }
    int get_sum(int x)
    {
        return sum[find(x)];
    }
    int get_size(int x)
    {
        return children[find(x)] + 1;
    }
    void print()
    {
        printf("  ");
        for (int i = 1; i < max_size; ++i)
        {
            printf(" [%d] ", i);
        }
        printf("\np:");
        for (int i = 1; i < max_size; ++i)
        {
            printf("  %d  ", p[i]);
        }
        printf("\ns:");

        for (int i = 1; i < max_size; ++i)
        {
            printf("  %d  ", sum[i]);
        }
        printf("\nc:");
        for (int i = 1; i < max_size; ++i)
        {
            printf("  %d  ", children[i]);
        }
        printf("\n");
    }
};

int main()
{
    int n, m;
    while (scanf("%d %d", &n, &m) != EOF)
    {
        union_find uf(n);
        for (int i = 0; i < m; ++i)
        {
            int o, p, q;
            scanf("%d", &o);
            switch(o)
            {
                case 1:
                    scanf("%d %d", &p, &q);
                    uf.unite(p, q);
                    break;
                case 2:
                    scanf("%d %d", &p, &q);
                    uf.move(p, q);
                    break;
                case 3:
                    scanf("%d", &p);
                    printf("%d %d\n", uf.get_size(p), uf.get_sum(p));
                    break;
                case 4:
                    uf.print();
                    break;
            }
        }
    }
}

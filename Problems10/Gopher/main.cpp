#include <cstdlib>
#include <cstdio>
#include <vector>
#include <stack>
#include <queue>
#include <cmath>
using namespace std;

struct flow_network {

    struct edge {
        int u, v, cap;
        edge *rev;
        bool forward;
        edge(int _u, int _v, int _cap, bool forw)
        : u(_u), v(_v), cap(_cap), forward(forw) { }
    };

    int n;
    vector<vector<edge*> > adj;
    flow_network(int _n) : n(_n), adj(_n) { }

    void add_edge(int u, int v, int cap) {
        edge *e = new edge(u, v, cap, true);
        edge *rev = new edge(v, u, 0, false);
        e->rev = rev;
        rev->rev = e;
        adj[u].push_back(e);
        adj[v].push_back(rev);
    }

    int augment(int s, int t) {
        vector<edge*> back(n, (edge*)0);
        queue<int> Q;
        Q.push(s);
        back[s] = (edge*)1;
        while (!Q.empty()) {
            int u = Q.front(); Q.pop();
            for (int i = 0; i < adj[u].size(); i++) {
                int v = adj[u][i]->v;
                if (back[v] == NULL && adj[u][i]->cap > 0) {
                    back[v] = adj[u][i];
                    Q.push(v);
                }
            }
        }

        if (back[t] == NULL)
            return 0;

        stack<edge*> S;
        S.push(back[t]);
        int bneck = back[t]->cap;
        while (S.top()->u != s) {
            S.push(back[S.top()->u]);
            bneck = min(bneck, S.top()->cap);
        }

        while (!S.empty()) {
            S.top()->cap -= bneck;
            S.top()->rev->cap += bneck;
            S.pop();
        }

        return bneck;
    }

    int max_flow(int source, int sink) {
        int flow = 0;
        while (true) {
            int f = augment(source, sink);
            if (f == 0) {
                break;
            }

            flow += f;
        }

        return flow;
    }
};

double get_dist(pair<double, double> a, pair<double, double> b)
{
    double x = abs(a.first - b.first);
    double y = abs(a.second - b.second);
    return sqrt(x*x + y*y);
}

int main()
{
    int n, m, s, v;
    while (scanf("%d %d %d %d\n", &n, &m, &s, &v) != EOF)
    {
        int SOURCE = 0,
        SINK = 1,
        LEFT = 2,
        RIGHT = LEFT + n,
        CNT = RIGHT + m;
        int reach = s*v;

        flow_network g(CNT);

        vector<pair<double, double> > gophers(n);
        vector<pair<double, double> > holes(m);

        for (int i = 0; i < n; ++i)
        {
            double x, y;
            scanf("%lf %lf", &x, &y);
            gophers[i] = make_pair(x, y);
        }
        for (int i = 0; i < m; ++i)
        {
            double x, y;
            scanf("%lf %lf", &x, &y);
            holes[i] = make_pair(x, y);
        }
        for (int i = 0; i < n; ++i)
        {
            for (int j = 0; j < m; ++j)
            {
                if (get_dist(gophers[i], holes[j]) <= reach)
                    g.add_edge(LEFT + i, RIGHT + j, 1);
            }
        }

        for (int i = 0; i < n; i++) {
            g.add_edge(SOURCE, LEFT + i, 1);
        }

        for (int i = 0; i < m; i++) {
            g.add_edge(RIGHT + i, SINK, 1);
        }


        printf("%d\n", n - g.max_flow(SOURCE, SINK));
    }

    return 0;
}

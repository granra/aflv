#include <cstdio>
#include <cmath>

using namespace std;

int main()
{
    int n, count = 0;
    while (scanf("%d", &n) != EOF)
    {
        double ans = log10((double)3) + (n*log10((double)3/2)) + 1;
        printf("Case %d: %.0lf\n", ++count, floor(ans));
    }
}

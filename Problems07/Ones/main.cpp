#include <cstdio>

using namespace std;

int main()
{
    int n;
    while (scanf("%d", &n) != EOF)
    {
        int ones = 1, number = 1 % n;
        for (; number != 0; number = (number * 10 + 1) % n, ones++);
        printf("%d\n", ones);
    }
}

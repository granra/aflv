#include <cstdio>
#include <vector>

using namespace std;

int main()
{
    int n, m;
    while (scanf("%d %d", &n, &m) != EOF)
    {
        vector<bool> outcomes(n + 1);
        vector<int> moves(m);
        for (int i = 0; i < m; ++i)
        {
            int x;
            scanf("%d", &x);
            moves[i] = x;
        }
        outcomes[0] = 1;
        for (int i = 1; i <= n; ++i)
        {
            bool set = 0;
            for (int j = 0; j < m; ++j)
            {
                if (i - moves[j] >= 0 && outcomes[i - moves[j]])
                {
                    outcomes[i] = 0;
                    set = 1;
                    break;
                }
            }
            if (!set) outcomes[i] = 1;
        }
        if (!outcomes[n]) printf("Stan wins\n");
        else              printf("Ollie wins\n");
    }
}

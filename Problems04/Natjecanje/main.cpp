#include <stdio.h>
#include <vector>

using namespace std;

int main()
{
    int n, s, r;
    scanf("%d %d %d", &n, &s, &r);
    vector<int> arr(n);
    for (int i = 0; i < n; ++i)
    {
        arr[i] = 1;
    }
    for (int i = 0; i < s; ++i)
    {
        int j;
        scanf("%d", &j);
        arr[--j]--;
    }
    for (int i = 0; i < r; ++i)
    {
        int j;
        scanf("%d", &j);
        arr[--j]++;
    }
    int count = 0;
    for (int i = 0; i < n; ++i)
    {
        if (arr[i] == 0)
        {
            if (i != 0 && arr[i - 1] > 1)
            {
                arr[i]++;
                arr[i - 1]--;
            }
            else if (i + 1 != n && arr[i + 1] > 1)
            {
                arr[i]++;
                arr[i + 1]--;
            }
            else count++;
        }
    }
    printf("%d\n", count);
}

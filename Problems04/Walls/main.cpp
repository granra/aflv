#include <cstdio>
#include <cmath>
#include <vector>

using namespace std;

bool reached[4] = {0, 0, 0, 0};

bool allWallsReached()
{
    for (int i = 0; i < 4; ++i)
        if (!reached[i]) return 0;
    return 1;
}

double distance(pair<double, double> wall, pair<int, int> crane)
{
    double x = abs(wall.first - crane.first);
    double y = abs(wall.second - crane.second);
    return sqrt((x*x) + (y*y));
}

bool reachesAny(vector<pair<double, double> > &walls, pair<int, int> crane, int r)
{
    int count = 0;
    for (int i = 0; i < 4; ++i)
    {
        if (distance(walls[i], crane) <= r)
            count++;
    }
    return count > 0;
}

void backtrack(vector<pair<double, double> > &walls, vector<pair<int, int> > &cranes,
                int r, int &min, int at, int end, int curr) {
    if (allWallsReached())
    {
        if (curr < min)
            min = curr;
        return;
    }
    if (at == end)
        return;

    if (reachesAny(walls, cranes[at], r))
    {
        bool changed[4];

        for (int i = 0; i < 4; ++i)
        {
            if (distance(walls[i], cranes[at]) <= r && !reached[i])
            {
                reached[i] = 1;
                changed[i] = 1;
            }
            else changed[i] = 0;
        }
        backtrack(walls, cranes, r, min, at + 1, end, curr + 1);

        // undo
        for (int i = 0; i < 4; ++i)
        {
            if (changed[i])
                reached[i] = 0;
        }
    }
    backtrack(walls, cranes, r, min, at + 1, end, curr);
}

vector<pair<double, double> > getWalls(double l, double w)
{
    vector<pair<double, double> > walls(4);
    walls[0] = pair<double, double>(-l/2, 0);
    walls[1] = pair<double, double>(l/2, 0);
    walls[2] = pair<double, double>(0, -w/2);
    walls[3] = pair<double, double>(0, w/2);
    return walls;
}

int main()
{
    double l, w;
    int n, r;
    while (scanf("%lf %lf %d %d", &l, &w, &n, &r) != EOF)
    {
        vector<pair<double, double> > walls = getWalls(l, w);
        vector<pair<int, int> > cranes(n);

        for (int i = 0; i < n; ++i)
        {
            int x, y;
            scanf("%d %d", &x, &y);
            cranes[i] = pair<int, int>(x, y);
        }
        int min = n + 1;
        backtrack(walls, cranes, r, min, 0, n, 0);
        if (min != n + 1)   printf("%d\n", min);
        else                printf("Impossible\n");
    }
}

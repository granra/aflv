#include <iostream>

const int SIZE = 8;

bool isValid(bool board[][SIZE], int a, int b)
{
  // Row and col search
  for (int i = 0; i < SIZE; ++i)
  {
    if (board[a][i] && i != b) return 0;
    if (board[i][b] && i != a) return 0;
  }

  // Diagonal searches
  for (int i = a + 1, j = b + 1; i < SIZE && j < SIZE; ++i, ++j)
    if (board[i][j]) return 0;

  for (int i = a - 1, j = b - 1; i >= 0 && j >= 0; --i, --j)
    if (board[i][j]) return 0;

  for (int i = a + 1, j = b - 1; i < SIZE && j >= 0; ++i, --j)
    if (board[i][j]) return 0;

  for (int i = a - 1, j = b + 1; i >= 0 && j < SIZE; --i, ++j)
    if (board[i][j]) return 0;

  return 1;
}

void printBoard(bool board[][SIZE])
{
  for (int i = 0; i < SIZE; ++i)
  {
    std::cout << "---------------------------------" << std::endl;
    for (int j = 0; j < SIZE; ++j)
    {
      std::cout << "| ";
      if (board[i][j])  std::cout << "X ";
      else              std::cout << "  ";
    }
    std::cout << "|" << std::endl;
  }
  std::cout << "---------------------------------" << std::endl;
}

int main()
{
  bool board[SIZE][SIZE];
  int count = 0;

  for (int i = 0; i < SIZE; ++i)
  {
    std::string line;
    std::getline(std::cin, line);
    for (int j = 0; j < SIZE; ++j)
    {
      if (line[j] == '*')
      {
        board[i][j] = 1;
        count++;
      }
      else board[i][j] = 0;
    }
  }

  if (count != 8)
  {
    std::cout << "invalid" << std::endl;
    return 0;
  }

  for (int i = 0; i < SIZE; ++i)
  {
    for (int j = 0; j < SIZE; ++j)
    {
      if (board[i][j])
      {
        if (!isValid(board, i, j))
        {
          std::cout << "invalid" << std::endl;
          return 0;
        }
      }
    }
  }

  std::cout << "valid" << std::endl;
  return 0;
}

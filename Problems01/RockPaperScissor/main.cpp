#include <iostream>

int getWinner(int A, int B, std::string Amove, std::string Bmove)
{
  if (Amove == "rock")
  {
    if      (Bmove == "rock")       return -1;
    else if (Bmove == "paper")      return B;
    else  /*(Bmove == "scissors")*/ return A;
  }
  else if (Amove == "paper")
  {
    if      (Bmove == "rock")       return A;
    else if (Bmove == "paper")      return -1;
    else  /*(Bmove == "scissors")*/ return B;
  }
  else /*(Amove == "scissors")*/
  {
    if      (Bmove == "rock")       return B;
    else if (Bmove == "paper")      return A;
    else  /*(Bmove == "scissors")*/ return -1;
  }
}

int main()
{
  int n, k;

  std::cin >> n;

  while (n != 0)
  {
    std::cin >> k;
    int h = ((k*n)*(n - 1))/2;
    int* wins = new int[n];
    int* draws = new int[n];

    for (int i = 0; i < n; ++i)
    {
      wins[i] = 0;
      draws[i] = 0;
    }

    for (int i = 0; i < h; ++i)
    {
      int A, B;
      std::string Amove, Bmove;
      std::cin >> A >> Amove;
      std::cin >> B >> Bmove;

      int winner = getWinner(A, B, Amove, Bmove);

      if (winner != -1) wins[winner - 1] += 1;
      else
      {
        draws[A - 1] += 1;
        draws[B - 1] += 1;
      }
    }
    std::cout.precision(3);
    std::cout << std::fixed;
    for (int i = 0; i < n; ++i)
    {
      if ((k*(n - 1)) - draws[i] > 0)
        std::cout << static_cast<double>(wins[i])/((k * (n - 1)) - draws[i]) << std::endl;
      else
        std::cout << "-" << std::endl;
    }

    delete[] wins;
    delete[] draws;
    std::cout << std::endl;
    std::cin >> n;
  }
}

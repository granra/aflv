#include <iostream>

std::string letters[27] = {"2", "22", "222", "3", "33", "333",
                           "4", "44", "444", "5", "55", "555",
                           "6", "66", "666", "7", "77", "777", "7777",
                           "8", "88", "888", "9", "99", "999", "9999",
                           "0"};

int main()
{
  int n;
  std::cin >> n;
  std::cin.ignore();

  for (int i = 0; i < n; ++i)
  {
    std::string line;
    std::getline(std::cin, line);

    std::cout << "Case #" << i + 1 << ": ";

    std::string last = "";
    for (int j = 0; j < line.length(); ++j)
    {
      std::string curr;
      if (line[j] == ' ') curr = letters[26];
      else                curr = letters[line[j] - 97];
      if (curr[0] == last[0]) std::cout << " ";
      std::cout << curr;
      last = curr;
    }
    std::cout << std::endl;
  }
}

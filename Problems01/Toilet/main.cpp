#include <stdio.h>

using namespace std;

void flip(char &current)
{
  switch(current)
  {
    case 'U':
      current = 'D';
      break;
    case 'D':
      current = 'U';
      break;
  }
}

int countMoves(char policy, char &current, char prefers)
{
  int count = 0;
  if (current != prefers)
  {
    count++;
    current = prefers;
  }

  if (current != policy && !(policy == 'X'))
  {
    count++;
    current = policy;
  }

  return count;
}

int main()
{
  char str[1001];
  scanf("%s1000", str);
  int countA = 0, countB = 0, countC = 0;
  char currentA = str[0], currentB = str[0], currentC = str[0];
  for (int i = 1; str[i] != '\0'; ++i)
  {
    countA += countMoves('U', currentA, str[i]);
    countB += countMoves('D', currentB, str[i]);
    countC += countMoves('X', currentC, str[i]);
  }

  printf("%d\n%d\n%d\n", countA, countB, countC);
}

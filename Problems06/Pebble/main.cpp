#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>

using namespace std;

int pebbles;
int mem[8388610];
void move(int i, char c)
{
    pebbles &= ~(1 << i);
    if (c == '-')
    {
        pebbles &= ~(1 << (i - 1));
        pebbles |= 1 << (i - 2);
    }
    else if (c == '+')
    {
        pebbles &= ~(1 << i + 1);
        pebbles |= 1 << (i + 2);
    }
}
void undo(int i, char c)
{
    pebbles |= 1 << i;
    if (c == '-')
    {
        pebbles |= 1 << (i - 1);
        pebbles &= ~(1 << (i - 2));
    }
    else if (c == '+')
    {
        pebbles |= 1 << (i + 1);
        pebbles &= ~(1 << (i + 2));
    }
}
bool hasPebble(int i)
{
    return pebbles & (1 << i);
}
int opt(int j)
{
    if (j > 22) return 0;

    if (mem[pebbles] != -1) return mem[j];

    int res = 0;
    for (int i = 0; i < 23; ++i)
    {
        if(hasPebble(i))
        {
            if (i > 1 && hasPebble(i - 1) && !hasPebble(i - 2))
            {
                move(i, '-');
                res = max(res, 1 + opt(j + 1));
                undo(i, '-');
            }
            if (i < 21 && hasPebble(i + 1) && !hasPebble(i + 2))
            {
                move(i, '+');
                res = max(res, 1 + opt(j + 1));
                undo(i, '+');
            }
        }
    }
    mem[pebbles] = res;
    return res;
}

int main()
{
    int n;
    scanf("%d", &n);
    for (int i = 0; i < n; ++i)
    {
        memset(mem, -1, sizeof(mem));
        pebbles = 0;
        int nr_of_pebbles = 0;
        for (int j = 0; j < 23; ++j)
        {
            char c;
            scanf(" %c", &c);
            if (c == 'o')
            {
                pebbles |= (1 << j);
                nr_of_pebbles++;
            }
        }
        int count = nr_of_pebbles - opt(0);
        printf("%d\n", count);
    }
}

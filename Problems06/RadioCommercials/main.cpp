#include <cstdio>
#include <algorithm>
#include <cstring>

using namespace std;

int arr[100000];
int mem[100000];
bool comp[100000];
int max_sum(int i) {
    if (i == 0) {
        return arr[i];
    }
    if (comp[i]) {
        return mem[i];
    }
    int res = max(arr[i], arr[i] + max_sum(i - 1));
    mem[i] = res;
    comp[i] = true;
    return res;
}

int main()
{
    memset(comp, 0, sizeof(comp));
    int N, P;
    while (scanf("%d %d", &N, &P) != EOF)
    {
        for (int i = 0; i < N; ++i)
        {
            int b;
            scanf("%d", &b);
            arr[i] = b - P;
        }
        int maximum = 0;
        for (int i = 0; i < N; i++) {
            maximum = max(maximum, max_sum(i));
        }
        printf("%d\n", maximum);
    }
}

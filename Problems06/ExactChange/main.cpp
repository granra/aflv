#include <cstdio>
#include <algorithm>
#include <vector>
#include <cstring>

using namespace std;

int encode(int16_t a, int16_t b)
{
    return (a << 16) | b;
}
void decode(int input, int &a, int &b)
{
    int mask = 0xFFFF;
    a = input >> 16;
    b = input & mask;
}

int INF = 0x7FFFFFFF; // int max
int d[105];
int mem[105][10005];
int opt(int i, int x, int a, int q) {
    if (x <= 0) return encode(a, q);
    if (i == -1) return INF;

    if (mem[i][x] != -1) return mem[i][x];

    int res = INF;
    res = min(res, opt(i - 1, x - d[i], a + d[i], q + 1));
    res = min(res, opt(i - 1, x, a, q));

    mem[i][x] = res;

    return res;
}

int main()
{
    int t;
    scanf("%d", &t);
    for (int i = 0; i < t; ++i)
    {
        memset(mem, -1, sizeof(mem));
        int x, n;
        scanf("%d %d", &x, &n);
        for (int j = 0; j < n; ++j)
        {
            int h;
            scanf("%d", &h);
            d[j] = h;
        }
        sort(d, d + n);
        int num = opt(n - 1, x, 0, 0);
        int a, b;
        decode(num, a, b);
        printf("%d %d\n", a, b);
    }
}

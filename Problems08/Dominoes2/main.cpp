#include <cstdio>
#include <vector>

using namespace std;

vector<int> adj[10000];
vector<bool> visited(10000, false);
void dfs(int u) {
    if (visited[u]) {
        return;
    }

    visited[u] = true;

    for (int i = 0; i < adj[u].size(); i++) {
        int v = adj[u][i];
        dfs(v);
    }
}
int main()
{
    int t;
    while (scanf("%d", &t) != EOF)
    {
        for (int i = 0; i < t; ++i)
        {
            int n, m, l;
            scanf("%d %d %d", &n, &m, &l);
            int count = 0;
            for (int j = 0; j < m; ++j)
            {
                int x, y;
                scanf("%d %d", &x, &y);
                x--; y--;
                adj[x].push_back(y);
            }
            for (int j = 0; j < l; ++j)
            {
                int x;
                scanf("%d", &x);
                x--;
                dfs(x);
            }
            for (int j = 0; j < n; ++j)
                if (visited[j]) count++;

            printf("%d\n", count);
            for (int j = 0; j < 10000; ++j)
            {
                adj[j].clear();
                visited[j] = 0;
            }
        }
    }
}

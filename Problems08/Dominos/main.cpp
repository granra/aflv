#include <cstdio>
#include <vector>

using namespace std;

vector<int> adj[100000];
vector<bool> visited(100000, false);
vector<bool> sort_visited(100000, false);
vector<int> order;
void dfs(int u) {
    if (visited[u]) {
        return;
    }

    visited[u] = true;

    for (int i = 0; i < adj[u].size(); i++) {
        int v = adj[u][i];
        dfs(v);
    }
}
void topsort(int u) {
    if (sort_visited[u]) {
        return;
    }

    sort_visited[u] = true;

    for (int i = 0; i < adj[u].size(); i++) {
        int v = adj[u][i];
        topsort(v);
    }
    order.push_back(u);
}
int main()
{
    int t;
    while (scanf("%d", &t) != EOF)
    {
        for (int i = 0; i < t; ++i)
        {
            int n, m;
            scanf("%d %d", &n, &m);
            for (int j = 0; j < m; ++j)
            {
                int x, y;
                scanf("%d %d", &x, &y);
                x--; y--;
                adj[x].push_back(y);
            }
            for (int u = 0; u < n; u++) {
                topsort(u);
            }
            int count = 0;
            for (int j = order.size() - 1; j >= 0; --j)
            {
                int x = order[j];
                if (!visited[x])
                {
                    dfs(x);
                    count++;
                }
            }
            printf("%d\n", count);
            for (int j = 0; j < n; ++j)
            {
                adj[j].clear();
                visited[j] = 0;
                sort_visited[j] = 0;
            }
            order.clear();
        }
    }
}
